Android Training
================
9 Tails
An Application To Demonstrate Lessons Learnt

#Course Outline - Wednesday July 26th 2017
1. Android Basics Blocks <br>
   a. Developing for mobile <br/>
   b. Activities <br/>
   c. Activity Life Cycle <br/>
   d. Intents  <br/>
2. Basic UI elements
   a. Layouts <br/>
   b. Input controls <br/>
3. Strings.xml & message localization <br/>

# - Lessons Learnt Overview -
  - Understanding Activity Life Cycle (Focus of the Day)
  - And Activity


